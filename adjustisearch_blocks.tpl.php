<?php

/**
 * @file adjustisearch_blocks.tpl.php
 * Template that controls the display of the Adjusti-Search blocks.
 *
 * Available variables:
 *   - $title: The title of the search block. Corresponds to the "Search
 *       Block Title" configuration option.
 *   - $searchform: The pre-rendered search form for the block.
 *
 * Tips for formatting:
 *   - Use divs and styling to refine the block output to what you want.
 *   - Since the form is pre-rendered by Drupal itself to retain functionality, you'll need to
 *     set up CSS to take advantage of this. Below is the general structure of the output from
 *     the pre-rendered form to make it easier for you to use structured CSS to style:
 *
 *   <form>
 *     <div>
 *       <div>
 *         <input type='text' />
 *       </div>
 *       <div>
 *         <select></select>
 *       </div>
 *       <input type='submit' />
 *     </div>
 *   </form>
 *
 *   If you are using the radio-option type instead for the search types instead of the drop-down,
 *   the div above that contains the <select> will instead contain the following, which repeats for
 *   each radio button that appears:
 *
 *         <div>
 *           <label>
 *             <input type='radio' />
 *           </label>
 *         </div>
 */
?>
<div>
  <?php if ($title): ?>
    <h2><?php print $title; ?></h2>
    <br />
  <?php endif; ?>
  <?php print $searchform; ?>
</div>