
/**
 * @file
 * Javascript used by the Adjusti-Search module.
 *
 * @ingroup adjustisearch
 */

/**
 * Redirects the form to where it needs to go based on the selected url.
 *
 * @return
 *   False if we're opening a new window, True otherwise.
 */
$(document).ready(function () {
  $('#edit-adjustisearch-search-submit').click(function () {
    if (adjustisearch_newEnabled == 1) {
      if (adjustisearch_is_array(adjustisearch_urlList)) {
        var url = adjustisearch_urlList[$('input[name="adjustisearch_search_type"]:checked').val()];
        // Do this if it is a dropdown instead of radio buttons.
        if (url == undefined) {
            url = adjustisearch_urlList[$('select[name="adjustisearch_search_type"] option:selected').val()];
        }
        
        var searchstring = encodeURIComponent($('input[name="adjustisearch_search_input"]').val());
        url = url.replace('%search%', searchstring);

        if (url.charAt(0) == '/') {
          return true;
        }
        else {
          window.open(url, 'newwin');
          return false;
        }
      }
      else {
        return true;
      }
    }
    else {
      return true;
    }
  });
});


/**
 * Helper function that checks to see if the value is an array.
 *
 * @param list
 *   variable to be checked for being an array.
 * @return
 *   Boolean representing whether the variable is an array or not.
 */
function adjustisearch_is_array(list) {
  return (list.constructor == Array);
}


/**
 * Sets the text field based on focus being gained on the field.
 *
 * @param formField
 *   The text field the function is applied to.
 */
function adjustisearch_on_focus(formField) {
  formField.style.fontStyle = 'normal';
  
  if (formField.value == adjustisearch_defaultText) {
    formField.value = '';
  }
}


/**
 * Sets the text field based on focus being lost on the field.
 *
 * @param formField
 *   The text field the function is applied to.
 */
function adjustisearch_on_blur(formField) {
  if (formField.value == '') {
    formField.style.fontStyle = 'italic';
    formField.value = adjustisearch_defaultText;
  }
}