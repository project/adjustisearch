
-- SUMMARY --

The Adjusti-Search module creates a block within your Drupal installation that
allows you to integrate as many of the searches used on your site into one
block. You also have the ability to add external searches into the search
block as long as the search takes its query through the URL.

For a full description of the module, visit the project page:
  http://drupal.org/project/adjustisearch

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/adjustisearch


-- REQUIREMENTS --

There are no requirements.


-- INSTALLATION --

Install like any other contributed module. See http://drupal.org/node/70151
for further information.


-- CUSTOMIZATION --

* Customize the Adjusti-Search block in Administer >> Site configuration >>
  Adjusti-Search Settings.
  
  - Search Block Title
    
    The title of the block. This is used to denote the block on a page.
    
  - Site Search List
    
    The list of search URLs to be used by the block. Each URL must be in the
    Adjusti-Search URL Format (ASUF) for the module to interpret it correctly.
    More information about ASUF can be found in Administer >> Help >>
    Adjusti-Search.
    
  - Search Box Width
    
    The width, in characters, you want the search box to be within the block.
    This is good if you want to change it without having to deal with CSS or
    theming.
    
  - Search Box Default Text
  
    Place a default text string in the search box when the block loads. This
    text will disappear when the search box gains focus. It will also
    reappear when the search box loses focus and it is blank.
    
  - Search List Type
  
    Select how you want the search types to be displayed in the block. The
    options for this setting are to either display as a drop-down list or to
    display as a set of radio buttons. The default is the drop-down list.
    
  - Limited Search Memory
    
    Check this box if you want the block to keep the last-used drop-down
    option that was selected when a search redirects to somewhere within your
    Drupal installation.
    
  - Open External Searches In A New Window
    
    Check this box if you want the module to open searches to external sites
    in a new window. This will keep the current page in the browser when they
    submit their search, but a new window will open if the search type that
    they have selected goes to an outside site (ie the url for the search does
    not begin with a '/'). If JavaScript is not enabled in a user's browser, 
    this option will not work.


-- TROUBLESHOOTING --

* If the block does not display but is enabled, check to make sure that the
  Site Search List in the module settings has at least one valid ASUF URL.

* If the drop-down menu of searches isn't appearing in the block, check to
  make sure that there is more than one valid ASUF URL in the Site Search
  List.

* If a search isn't showing up in the drop-down for the block, make sure
  that it is an ASUF URL in the Site Search List.


-- CONTACT --

Current maintainers:
* Connor Kelly (ConnorK) - http://drupal.org/user/580556